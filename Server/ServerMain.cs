using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class ServerMain 
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Missing parameter MaxTaskCount");
                Console.ReadKey();
                return;
            }

            if (Int32.TryParse(args[0], out var taskCount) && taskCount < 1 && taskCount > 10)
            {
                Console.WriteLine("Invalid parameter MaxTaskCount \n It must be integer from 1 to 10 ");
                Console.ReadKey();
                return;
            }

            IServer s = new Server(taskCount);
            s.Run();
            Console.WriteLine("Server is running! \n Press any key to stop");
            Console.ReadKey();
            s.Stop();
        }
    }
}
