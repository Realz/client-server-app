﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;

namespace Server
{
    class Palindrome : IMethod
    {
        private readonly List<Param> _params;
        private string _sentence;
        private readonly bool _isValid; 

        public Palindrome(List<Param> methodParams)
        {
            _params = methodParams;
            _isValid = ValidateParams();
        }

        public string Run()
        {
            string result = "";
            if (!_isValid)
                Console.WriteLine("Cannot run invalid method");
            else
                result = Calc() ? "True" : "False";
            return result;
        }

        public bool IsValid()
        {
            return _isValid;
        }

        private bool ValidateParams()
        {
            Param p = _params.Find(x => x.Name == "Sentence");
            if (p == null)
                return false;
            _sentence = p.Value;
            return true;
        }

        private bool Calc()
        {
            Thread.Sleep(1000);   
            _sentence = _sentence.ToLower();
            Regex rgx = new Regex("[^a-zа-я0-9]");
            _sentence = rgx.Replace(_sentence, "");
            return IsPalindrome(_sentence);
        }

        private bool IsPalindrome(string value)
        {
            int start = 0;
            int end = value.Length - 1;
            while (true)
            {
                if (start > end)
                    return true;
                if (value[start] != value[end])
                    return false;
                start++;
                end--;
            }
        }
    }
}
