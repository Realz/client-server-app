﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    class Response
    {
        private string _code;
        private string _value;

        public Response(string code, string value)
        {
            this._code = code;
            this._value = value;
        }

        public string Code => _code;
        public string Value => _value;
    }
}
