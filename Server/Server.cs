﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace Server
{
    enum MethodType
    {
        Palindrome,
        None
    }

    class Server : IServer
    {
        private const int MaxConnectionsCount = 1000;
        private const int Port = 8000;
        private bool _isRunning = false;
        private JSchema _schema = JSchema.Parse(@"{
            'type' : 'object',
            'properties' : {
                'Method' : {'type' : 'string'},
                'Params' : {'type' : 'array' , 'items' : { 
                        'type' : 'object', 'properties' : {
                            'Name' : {'type' : 'string'},
                            'Value' : {'type' : 'string'}
                         }
                    }
                }
            }
        }");
        private Socket _listenSocket;
        private int _taskCount;
        private readonly int _maxTaskCount;
        private readonly Mutex _mutex;

        public Server(int maxTaskCount)
        {
            _maxTaskCount = maxTaskCount;
            _taskCount = 0;
            _mutex = new Mutex();
        }

        public void Run()
        {
            if (_isRunning)
            {
                Console.WriteLine("Server already running");
                return;
            }

            _isRunning = true;
            _listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint localhost = new IPEndPoint(IPAddress.Parse("127.0.0.1"), Port);
            try
            {
                _listenSocket.Bind(localhost);
                _listenSocket.Listen(MaxConnectionsCount);
                Task handlerTask = new Task(HandlerLoop);
                handlerTask.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error was occured");
                Console.WriteLine(e.StackTrace);
            }
        }

        public void Stop()
        {
            if (!_isRunning)
                Console.WriteLine("Server is NOT running");
            else
                _isRunning = false;
        }

        private void HandlerLoop()
        {
            while (_isRunning)
            {
                Socket handler = _listenSocket.Accept();
                if (_taskCount >= _maxTaskCount)
                {
                    SendResponse(handler, new Response("ERROR", "Server is busy"));
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                    continue;
                }

                _mutex.WaitOne();
                _taskCount++;
                _mutex.ReleaseMutex();
                
                Task handleTask = new Task(() => Handle(handler));
                handleTask.Start();
            }
        }

        private void Handle(Socket handler)
        {
            JObject json = JObject.Parse(GetRequest(handler));

            if (!json.IsValid(_schema))
                SendResponse(handler, new Response("ERROR", "Invalid JSON format"));
            else
            {
                Request request = JsonConvert.DeserializeObject<Request>(json.ToString());
                Process(request, handler);
            }
            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
            _mutex.WaitOne();
            _taskCount--;
            _mutex.ReleaseMutex();
        }

        private string GetRequest(Socket handler)
        {
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            byte[] data = new byte[256];
            do
            {
                bytes = handler.Receive(data);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (handler.Available > 0);
            Console.WriteLine(DateTime.Now.ToShortTimeString() + " - INPUT json: " + builder.ToString());
            return builder.ToString();
        }

        private void Process(Request request, Socket handler)
        {
            MethodType inputMethod = ValidateMethod(request.Method);
            IMethod method;
            switch (inputMethod)
            {
                case MethodType.Palindrome:
                    method = new Palindrome(request.Params);
                    break;
                case MethodType.None:
                    SendResponse(handler, new Response("ERROR", "Not implemented method"));
                    return;
                default:
                    Console.WriteLine("Unexpected method");
                    return;
            }
            if (!method.IsValid())
            {
                SendResponse(handler, new Response("ERROR", "Invalid method " + request.Method + " params"));
                return;
            }
            String result = method.Run();
            SendResponse(handler , new Response("OK" , result));
        }

        private MethodType ValidateMethod(string name)
        {
            MethodType result;
            switch (name)
            {
                case "Palindrome": result = MethodType.Palindrome; break;
                default: result = MethodType.None; break;
            }
            return result;
        }

        private void SendResponse(Socket handler, Response response)
        {
            handler.Send(Encoding.Unicode.GetBytes(JsonConvert.SerializeObject(response)));
        }

    }
}
