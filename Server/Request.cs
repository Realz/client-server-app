﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Server
{
    class Request
    {
        private string _method;
        private List<Param> _params;

        public Request(string method, List<Param> methodParams)
        {
            Method = method;
            Params = methodParams;
        }

        public string Method
        {
            get { return _method; }
            set { _method = value; }
        }

        public List<Param> Params
        {
            get { return _params; }
            set { _params = value; }
        }

    }
}
