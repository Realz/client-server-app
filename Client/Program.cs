using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Client
{
    class Program
    {
        static int port = 8000; 
        static string address = "127.0.0.1";
        private static int maxFilesCount = 127;

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Missing parameter path");
                Console.ReadKey();
                return;
            }

            if (!Directory.Exists(args[0]))
            {
                Console.WriteLine("Directory not exist");
                Console.ReadKey();
                return;
            }

            string directoryPath = args[0];
            
            Console.WriteLine("<FileName> : <Response>");
            List<Task> tasks = new List<Task>(maxFilesCount);
            foreach (string file in Directory.EnumerateFiles(directoryPath, "*.txt"))
            {
                if (tasks.Count() >= maxFilesCount)
                    break;
                tasks.Add(new Task(() => ProcessFile(file)));
                tasks.Last().Start();
            }

            Task.WaitAll(tasks.ToArray());
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static string Request(byte[] data)
        {
            IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse(address), port);

            Socket socket;
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(ipPoint);
                socket.Send(data);
            }
            catch (Exception e)
            {
                return "Can't connect to server";
            }
            data = new byte[256];
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = socket.Receive(data, data.Length, 0);
                builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
            }
            while (socket.Available > 0);
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
            return builder.ToString();
        }

        private static void ProcessFile(string filepath)
        {
            string content = File.ReadAllText(filepath, Encoding.Default);
            JArray jArray = new JArray();
            jArray.Add(new JObject(
                new JProperty("Name", "Sentence"),
                new JProperty("Value", content)
            ));

            JObject jsonRequest = new JObject(
                new JProperty("Method", "Palindrome"),
                new JProperty("Params", jArray));
            byte[] data = Encoding.Unicode.GetBytes(jsonRequest.ToString());
            String serverResponse = Request(data);
            try
            {
                JObject response = JObject.Parse(serverResponse);
                Console.WriteLine(Path.GetFileName(filepath) + " : " + response.GetValue("Value"));
            }
            catch (Exception e)
            {
                Console.WriteLine(Path.GetFileName(filepath) + " : " + serverResponse);
            }

        }
    }
}
